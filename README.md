DevSnippets privacy policy

DevSnippets does not collect any user data.
DevSnippets does not need the user to create any account or transfer any data to the developer.
DevSnippets optionally uses for the "run code" functionality a free API from Jdoodle: https://www.jdoodle.com/

Author: Daniel Illescas Romero
